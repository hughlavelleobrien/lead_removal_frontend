import {Row, Form, Button} from "react-bootstrap"

function PatientForm({handleSubmit, inFlight}) {
  return (
    <Row className="InputForm">
      <Form onSubmit={handleSubmit}>
        <Form.Group>
          
          <Form.Label>Gender</Form.Label>
          
          <Form.Control
            as="select"
            className="mr-sm-2"
            name="gender"
            id="gender"
            custom
          >
            <option value="0">Male</option>
            <option value="1">Female</option>
          </Form.Control>
        </Form.Group>

        <hr />
        <Form.Group controlId="DwellTime">
          <Form.Label>Dwell Time (years)</Form.Label>
          <Form.Control name="dwellTime" type="number" defaultValue="10" />
          <Form.Text className="text-muted">
            Lead dwell time from implant to planned explant in years
          </Form.Text>
        </Form.Group>

        <hr />
        <Form.Group controlId="Age">
          <Form.Label>Age at Explant</Form.Label>
          <Form.Control type="number" name="age" defaultValue="70" />
        </Form.Group>

        <hr />
        <Form.Label>Extraction Indications</Form.Label>
        <div className="checkbox-list">
          <Form.Group controlId="Sepsis">
            <Form.Check name="sepsisOrEndocarditis" value={true} type="checkbox" label="Sepsis or Endocarditis" />
          </Form.Group>
          <Form.Group name="infectedDeviceOrErosion" controlId="InfectedDeviceOrErosion">
            <Form.Check type="checkbox" value={true} label="Infected device or erosion" />
          </Form.Group>
        </div>

        <hr />
        <Form.Label>Patient Co-Morbidities</Form.Label>
        <div className="checkbox-list">
          <Form.Group controlId="Diabetes">
            <Form.Check type="checkbox" name="diabetes" value={true} label="Diabetes" />
          </Form.Group>
          <Form.Group controlId="HeartFailure">
            <Form.Check type="checkbox" name="heartFailure" value={true} label="Heart Failure" />
          </Form.Group>
          <Form.Group controlId="Respiratory">
            <Form.Check type="checkbox" name="respiratory" value={true} label="Respiratory Condition" />
          </Form.Group>
          <Form.Group controlId="CAD">
            <Form.Check type="checkbox" name="CAD" value={true} label="Coronary Artery Disease" />
          </Form.Group>
        </div>
        <hr />
        <Form.Group controlId="eGFR">
          <Form.Label>eGFR (ml/min/m2)</Form.Label>
          <Form.Control min="0" type="number" name="eGFR" defaultValue="80" />
        </Form.Group>

        <hr /> 
        <Form.Group controlId="CRP">
          <Form.Label>CRP (mg/dL)</Form.Label>
          <Form.Control min="0" type="number" name="crp" defaultValue="5" />
        </Form.Group>

        <hr />
        <Form.Group controlId="LVEF">
          <Form.Label>Left Ventricle Ejection Fraction (%)</Form.Label>
          <Form.Control min="0" type="number" name="lvef" defaultValue="65" />
        </Form.Group>

        <hr />
        <Form.Label>ICD Leads</Form.Label>

        <div className="checkbox-list">

          <Form.Group controlId="ICDSingleCoil">
            <Form.Check type="checkbox" value={true} name="icdSingle" label="ICD: Single Coil" />
          </Form.Group>
          <Form.Group controlId="ICDDualCoil">
            <Form.Check type="checkbox" value={true} name="icdDual" label="ICD: Dual Coil" />  
          </Form.Group>
        </div>
        <hr />
        <Form.Group controlId="RepeatExtraction">
          <div className="checkbox-list">
            <Form.Check type="checkbox" value={true} name="repeatExtraction" label="Repeat Extaction" />
          </div>
        </Form.Group>
        
        { !inFlight &&
          <Button variant="primary" type="submit">
            Submit
          </Button>
        }
      </Form>
    </Row>
  )
}

export default PatientForm
