import {Card, Accordion} from "react-bootstrap"

function Result({result_score}) {

  if (!result_score) {
    return (<div />)
  }

  console.log(result_score)

  function scrollDown() {
    const description = document.getElementById("description");
    description.scrollIntoView()
  }

  if (result_score < -0.30) {
    return(
      <Accordion defaultActiveKey="0" onSelect={scrollDown}>
        <Card className="low-risk" border="success">
          <Accordion.Toggle onClick={scrollDown} as={Card.Header} eventKey="0">
            Low Risk
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="0">
            <Card.Body>
              <p> Low risk indicates the model has a >66% confidence this case would not result in a major complication.
              </p>
              <Explaination />
            </Card.Body>
          </Accordion.Collapse>
          <div id="description" />
        </Card>
      </Accordion>
    )
  } else if (result_score > 0.30) {
    return (
      <Accordion defaultActiveKey="0" onSelect={scrollDown}>
        <Card className="high-risk"border="danger">
          <Accordion.Toggle as={Card.Header} eventKey="0">
            High Risk
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="0">
            <Card.Body >
              <p>
                High risk indicates the model has identified this patient as at risk of a major complication with >60% confidence.
              </p>
              <Explaination />
            </Card.Body>
          </Accordion.Collapse>
          <div id="description" />
        </Card>
      </Accordion>
    )
  }

  return (
    <Accordion defaultActiveKey="0" onSelect={scrollDown}>
      <Card className="medium-risk" border="warning">
        <Accordion.Toggle as={Card.Header} eventKey="0">
          Medium Risk
        </Accordion.Toggle>
        <Accordion.Collapse eventKey="0">
          <Card.Body>
            <p>
              Medium risk signals the model has a low level of confidence in predicting this case.
            </p>
            <p>
              Probabilities of no incident vs major incident are within .3 of eachother
            </p>
            <Explaination />
          </Card.Body>
        </Accordion.Collapse>
        <div id="description" />
      </Card>
    </Accordion>
  )
}

function Explaination() {
  return (
    <p>
      Major complications were defined in the ELECTRa study as any complications related to the procedure that were life threatening or resulted in death, or any unexpected event that caused persistent or significant disability, or any event that required significant surgical intervention to prevent any of outcomes. <a href="https://doi.org/10.1093/eurheartj/ehx080"> (Original paper) </a>
    </p>
  )
}

export default Result
