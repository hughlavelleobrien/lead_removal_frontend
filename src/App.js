import React, { useState } from 'react';
import './App.css';
import {Spinner, Col, Row} from "react-bootstrap"
import PatientForm from "./patient_form.js"
import Result from "./result.js"

function App() {

  // TODO make env var
  const host = window.location.hostname
  let endpoint
  if (host === "127.0.0.1" || host === "localhost"){
    endpoint = 'http://127.0.0.1/predict'
  } else {
    endpoint = 'https://lead-removal.hugh-obrien.com/predict'
  }

  /* endpoint = 'https://lead-removal.hugh-obrien.com/predict' */
  
  const [inFlight, setInFlight] = useState(false);
  const [score, setScore] = useState(null);
  
  async function handleSubmit(event) {
    const formData = new FormData(event.target)
    const formDataObj = Object.fromEntries(formData.entries())
    event.preventDefault();
    setInFlight(true)

    const result = await fetch(endpoint, {
      method: 'POST',
      body: JSON.stringify(formDataObj),
    }).then(function(response) {
      return response.json()
    }).catch(function(err) {
      console.log(err)
      setInFlight(false)
    })
    setScore(result.score)
    setInFlight(false)
    window.scrollTo({ top: document.body.scrollHeight, behavior: 'smooth' })
  }
  
  return (
    <div className="App">
      <header className="App-header">
        Lead Removal Risk Prediction
      </header>
      <Col xs={12} md={12}>

        <PatientForm handleSubmit={handleSubmit} inFlight={inFlight} />
        
        { inFlight &&
          <Spinner animation="border" variant="primary" />
        }

        <br />
        
        <Row className="result-row">
          <Col xs={12} md={3} />
          <Col xs={12} md={6}>
            <Result result_score={score} />
          </Col>
          <Col xs={12} md={3} />
        </Row>
      </Col>

      <footer className="App-footer">
        <a href="http://cemrg.co.uk">
          <img id="logo" alt="cemrg logo" src="/logo.svg" />
        </a>
      </footer>
    </div> 
  )
}

export default App;
