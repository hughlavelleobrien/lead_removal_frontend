/* config-overrides.js */

module.exports = {
  devServer: function(configFunction) {
    return function(proxy, allowedHost) {
      // Create the default config by calling configFunction with the proxy/allowedHost parameters
      const config = configFunction(proxy, allowedHost);

      //config.watchOptions.ignored = [/^(?!\/app\/src\/).+\/node_modules\//g, /\.#.*/g]

      config.watchOptions.ignored = /^(?!\/app\/src\/)(.+\/node_modules\/)|(.*\.#.*)/g
      
      // Return your customised Webpack Development Server config.
      console.log(config)
      return config;
    }
  },
}

